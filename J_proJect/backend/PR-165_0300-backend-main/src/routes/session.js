const express = require('express');
const mongoose = require('mongoose');
const sessionModel = require('../models/sessionModel');

const router = express.Router();
router.get('/withMoniteurs', async (req, res) => {
    try {
        const result = await sessionModel.aggregate([
            {
                $lookup: {
                    from: 'moniteur',
                    localField: 'moniteurId',
                    foreignField: 'id',
                    as: 'moniteur'
                }
            },
            { $unwind: "$moniteur" }
        ]);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});
// Obtenir toutes les sessions
router.get('/', async (req, res) => {
    try {
        const sessions = await sessionModel.find();
        res.status(200).json(sessions);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Obtenir une session par ID
router.get('/:id', async (req, res) => {
    try {
        const session = await sessionModel.findById(req.params.id);
        if (session == null) {
            return res.status(404).json({ message: 'Session non trouvée' });
        }
        res.status(200).json(session);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Créer une nouvelle session
router.post('/', async (req, res) => {
    const session = new sessionModel({
        _id: new mongoose.mongo.ObjectId(),
        date: req.body.date,
        dureeM: req.body.dureeM,
        moniteurId: req.body.moniteurId,
        places: req.body.places,
        cour: {
            tarif: req.body.cour.tarif,
            niveau: req.body.cour.niveau,
            type: req.body.cour.type,
        },
        participations: req.body.participations
    });

    try {
        const nouvelleSession = await session.save();
        res.status(201).json(nouvelleSession);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Mettre à jour une session par ID
router.put('/:id', async (req, res) => {
    try {
        const session = await sessionModel.findById(req.params.id);
        if (session == null) {
            return res.status(404).json({ message: 'Session non trouvée' });
        }

        session.date = req.body.date ?? session.date;
        session.dureeM = req.body.dureeM ?? session.dureeM;
        session.moniteurId = req.body.moniteurId ?? session.moniteurId;
        session.places = req.body.places ?? session.places;
        session.cour.tarif = req.body.cour.tarif ?? session.cour.tarif;
        session.cour.niveau = req.body.cour.niveau ?? session.cour.niveau;
        session.cour.type = req.body.cour.type ?? session.cour.type;
        session.participations = req.body.participations ?? session.participations;

        const sessionMisAJour = await session.save();
        res.status(200).json(sessionMisAJour);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Supprimer une session par ID
router.delete('/:id', async (req, res) => {
    try {
        const session = await sessionModel.findById(req.params.id);
        if (session == null) {
            return res.status(404).json({ message: 'Session non trouvée' });
        }

        await session.deleteOne();
        res.status(200).json({ message: 'Session supprimée avec succès' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


module.exports = router;