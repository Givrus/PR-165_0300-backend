
const express = require('express');
const mongoose = require('mongoose');
const proprietaireModel = require('../models/proprietaireModel');

const router = express.Router();

// Obtenir tous les propriétaires
router.get('/', async (req, res) => {
    try {
        const proprietaires = await proprietaireModel.find();
        res.status(200).json(proprietaires);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Obtenir un propriétaire par ID
router.get('/:id', async (req, res) => {
    try {
        const proprietaire = await proprietaireModel.findById(req.params.id);
        if (proprietaire == null) {
            return res.status(404).json({ message: 'Propriétaire non trouvé' });
        }
        res.status(200).json(proprietaire);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Créer un nouveau propriétaire
router.post('/', async (req, res) => {
    const proprietaire = new proprietaireModel({
        _id: new mongoose.mongo.ObjectId(),
        adresse: req.body.adresse,
        certificateDelivre: req.body.certificateDelivre,
        dateNaissance: req.body.dateNaissance,
        nom: req.body.nom,
        origine: req.body.origine,
        prenom: req.body.prenom,
        chiens: req.body.chiens,
        localite: {
            localite: req.body.localite.localite,
            npa: req.body.localite.npa
        },
        mailProprietaires: req.body.mailProprietaires,
        telephoneProprietaires: req.body.telephoneProprietaires
    });

    try {
        const nouveauProprietaire = await proprietaire.save();
        res.status(201).json(nouveauProprietaire);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Mettre à jour un propriétaire par ID
router.put('/:id', async (req, res) => {
    try {
        const proprietaire = await proprietaireModel.findById(req.params.id);
        if (proprietaire == null) {
            return res.status(404).json({ message: 'Propriétaire non trouvé' });
        }

        proprietaire.adresse = req.body.adresse ?? proprietaire.adresse;
        proprietaire.certificateDelivre = req.body.certificateDelivre ?? proprietaire.certificateDelivre;
        proprietaire.dateNaissance = req.body.dateNaissance ?? proprietaire.dateNaissance;
        proprietaire.nom = req.body.nom ?? proprietaire.nom;
        proprietaire.origine = req.body.origine ?? proprietaire.origine;
        proprietaire.prenom = req.body.prenom ?? proprietaire.prenom;
        proprietaire.chiens = req.body.chiens ?? proprietaire.chiens;
        proprietaire.localite.localite = req.body.localite.localite ?? proprietaire.localite.localite;
        proprietaire.localite.npa = req.body.localite.npa ?? proprietaire.localite.npa;
        proprietaire.mailProprietaires = req.body.mailProprietaires ?? proprietaire.mailProprietaires;
        proprietaire.telephoneProprietaires = req.body.telephoneProprietaires ?? proprietaire.telephoneProprietaires;

        const proprietaireMisAJour = await proprietaire.save();
        res.status(200).json(proprietaireMisAJour);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Supprimer un propriétaire par ID
router.delete('/:id', async (req, res) => {
    try {
        const proprietaire = await proprietaireModel.findById(req.params.id);
        if (proprietaire == null) {
            return res.status(404).json({ message: 'Propriétaire non trouvé' });
        }

        await proprietaire.deleteOne();
        res.status(200).json({ message: 'Propriétaire supprimé avec succès' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;