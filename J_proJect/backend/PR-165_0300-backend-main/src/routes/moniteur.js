const express = require('express');
const mongoose = require('mongoose');
const moniteurModel = require('../models/moniteurModel');

const router = express.Router();
router.get('/withEcoles', async (req, res) => {
    try {
        const result = await moniteurModel.aggregate([
            {
                $lookup: {
                    from: 'ecole',
                    localField: 'ecoleId',
                    foreignField: 'id',
                    as: 'ecole'
                }
            },
            { $unwind: "$ecole" }
        ]);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Obtenir tous les moniteurs
router.get('/', async (req, res) => {
    try {
        const moniteurs = await moniteurModel.find();
        res.status(200).json(moniteurs);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});



// Obtenir un moniteur par ID
router.get('/:id', async (req, res) => {
    try {
        const moniteur = await moniteurModel.findById(req.params.id);
        if (moniteur == null) {
            return res.status(404).json({ message: 'Moniteur non trouvé' });
        }
        res.status(200).json(moniteur);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Créer un nouveau moniteur
router.post('/', async (req, res) => {
    const moniteur = new moniteurModel({
        _id: new mongoose.mongo.ObjectId(),
        adresse: req.body.adresse,
        civilite: req.body.civilite,
        dateNaissance: req.body.dateNaissance,
        ecoleId: req.body.ecoleId,
        nom: req.body.nom,
        prenom: req.body.prenom,
        localite: {
            localite: req.body.localite.localite,
            npa: req.body.localite.npa
        },
        mailMoniteurs: req.body.mailMoniteurs,
        telephoneMoniteurs: req.body.telephoneMoniteurs
    });

    try {
        const nouveauMoniteur = await moniteur.save();
        res.status(201).json(nouveauMoniteur);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Mettre à jour un moniteur par ID
router.put('/:id', async (req, res) => {
    try {
        const moniteur = await moniteurModel.findById(req.params.id);
        if (moniteur == null) {
            return res.status(404).json({ message: 'Moniteur non trouvé' });
        }

        moniteur.adresse = req.body.adresse ?? moniteur.adresse;
        moniteur.civilite = req.body.civilite ?? moniteur.civilite;
        moniteur.dateNaissance = req.body.dateNaissance ?? moniteur.dateNaissance;
        moniteur.ecoleId = req.body.ecoleId ?? moniteur.ecoleId;
        moniteur.nom = req.body.nom ?? moniteur.nom;
        moniteur.prenom = req.body.prenom ?? moniteur.prenom;
        moniteur.localite.localite = req.body.localite.localite ?? moniteur.localite.localite;
        moniteur.localite.npa = req.body.localite.npa ?? moniteur.localite.npa;
        moniteur.mailMoniteurs = req.body.mailMoniteurs ?? moniteur.mailMoniteurs;
        moniteur.telephoneMoniteurs = req.body.telephoneMoniteurs ?? moniteur.telephoneMoniteurs;

        const moniteurMisAJour = await moniteur.save();
        res.status(200).json(moniteurMisAJour);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});



// Supprimer un moniteur par ID
router.delete('/:id', async (req, res) => {
    try {
        const moniteur = await moniteurModel.findById(req.params.id);
        if (moniteur == null) {
            return res.status(404).json({ message: 'Moniteur non trouvé' });
        }

        await moniteur.deleteOne();
        res.status(200).json({ message: 'Moniteur supprimé avec succès' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});



module.exports = router;