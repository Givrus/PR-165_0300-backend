const express = require('express');
const mongoose = require('mongoose');
const ecoleModel = require('../models/ecoleModel');

const router = express.Router();

// Obtenir toutes les écoles (fonctionel)
router.get('/', async (req, res) => {
    try {
        const ecoles = await ecoleModel.find();
        res.status(200).json(ecoles);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Obtenir une école par ID (fonctionel)
router.get('/:id', async (req, res) => {
    try {
        const ecole = await ecoleModel.findById(req.params.id);
        if (ecole == null) {
            return res.status(404).json({ message: 'École non trouvée' });
        }
        res.status(200).json(ecole);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Créer une nouvelle école
router.post('/', async (req, res) => {
    const ecole = new ecoleModel({
        _id : new mongoose.mongo.ObjectId(),
        adresse: req.body.adresse,
        nom: req.body.nom,
        localite: {
            localite: req.body.localite.localite,
            npa: req.body.localite.npa
        },
        mailEcoles: req.body.mailEcoles,
        telephoneEcoles: req.body.telephoneEcoles
    });

    try {
        const nouvelleEcole = await ecole.save();
        res.status(201).json(nouvelleEcole);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Mettre à jour une école par ID
router.put('/:id', async (req, res) => {
    try {
        const ecole = await ecoleModel.findById(req.params.id);
        if (ecole == null) {
            return res.status(404).json({ message: 'École non trouvée' });
        }

        ecole.adresse = req.body.adresse ?? ecole.adresse;
        ecole.nom = req.body.nom ?? ecole.nom;
        ecole.localite.localite = req.body.localite.localite ?? ecole.localite.localite;
        ecole.localite.npa = req.body.localite.npa ?? ecole.localite.npa;
        ecole.mailEcoles = req.body.mailEcoles ?? ecole.mailEcoles;
        ecole.telephoneEcoles = req.body.telephoneEcoles ?? ecole.telephoneEcoles;

        const ecoleMisAJour = await ecole.save();
        res.status(200).json(ecoleMisAJour);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Supprimer une école par ID (fonctionel)
router.delete('/:id', async (req, res) => {
    try {
        const ecole = await ecoleModel.findById(req.params.id);
        if (ecole == null) {
            return res.status(404).json({ message: 'École non trouvée' });
        }

        await ecole.deleteOne();
        res.status(200).json({ message: 'École supprimée avec succès' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;