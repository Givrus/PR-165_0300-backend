const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 3000;

const ecoleRoutes = require('./routes/ecole');
const moniteurRoutes = require('./routes/moniteur');
const sessionRoutes = require('./routes/session');
const proprietaireRoutes = require('./routes/proprietaire');

main().catch(err => console.log(err));

async function main() {
  await mongoose.connect('mongodb://127.0.0.1:27017/cours_canin', {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  console.log("Connected to the database");

  app.use(express.json());

  app.get('/', (req, res) => {
    res.send("Welcome to the REST API");
  });

  app.use('/ecole', ecoleRoutes);
  app.use('/moniteur', moniteurRoutes);
  app.use('/session', sessionRoutes);
  app.use('/proprietaire', proprietaireRoutes);

  app.listen(port, () => {
    console.log(`App listening on port ${port}`);
  });
}