const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const sessionSchema = new Schema({
    "_id": mongoose.ObjectId,
    "date": Date,
    "dureeM": Number,
    "moniteurId": String,
    "places": Number,
    "cour":{
        "tarif": Number,
        "niveau": String,
        "type": {type:String},
    },
    "participations":[{
      "chienId": String,
      "avecProprietaire": Boolean,  
    }]
}, { collection: "session" })

module.exports = model("session", sessionSchema);