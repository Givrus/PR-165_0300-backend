const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const proprietaireSchema = new Schema({
    "_id": mongoose.ObjectId,
    "adresse": String,
    "certificateDelivre": Date,
    "dateNaissance": Date,
    "nom": String,
    "origine":String,
    "prenom":String,
    "chiens":[{
        "nom":String,
        "male": Boolean,
        "id": String,
        "identificationAmicus": String,
        "dateNaissance": String,
        "nom_race": String,
    }],
    "localite":{
        "localite":String,
        "npa": Number,
    },
    "mailProprietaires":[{
        "mail": String,
        "type": {type:String},
    }],
    "telephoneProprietaires":[{
        "numero": String,
        "type": {type:String},
    }]
}, { collection: "proprietaire" })

module.exports = model("proprietaire", proprietaireSchema);