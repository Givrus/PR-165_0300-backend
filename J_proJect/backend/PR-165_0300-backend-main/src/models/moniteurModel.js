const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const moniteurSchema = new Schema({
    "_id": mongoose.ObjectId,
    "adresse": String,
    "civilite" : String,
    "dateNaissance": Date,
    "ecoleId" : { type: mongoose.ObjectId, ref: 'ecole' },
    "nom": String,
    "prenom": String,
    "localite": 
    {
        "localite":String,
        "npa": Number
    },
    "mailMoniteurs": 
    [{
        "mail": String,
        "type": {type:String}
    }],
    "telephoneMoniteurs":[{
        "numero": String,
        "type": {type:String}
    }]
}, { collection: "moniteur" })

module.exports = model("moniteur", moniteurSchema);