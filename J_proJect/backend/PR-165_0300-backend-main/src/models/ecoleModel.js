const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const ecoleSchema = new Schema({
    "_id": mongoose.ObjectId,
    "adresse": String,
    "nom": String,
    "localite": 
    {
        "localite":String,
        "npa": Number
    },
    "mailEcoles": 
    [{
        "mail": String,
        "type": {type:String}
    }],
    "telephoneEcoles":[{
        "numero": String,
        "type": {type:String}
    }]
}, { collection: "ecole" })

module.exports = model("ecole", ecoleSchema);